require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
  end
  
  test "should get root" do
    #get static_pages_home_url  # Before Named Routes 
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end
  
  test "should get home" do
    #get static_pages_home_url  # Before Named Routes 
    get home_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end

  test "should get help" do
    #get static_pages_help_url  # Before Named Routes 
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end
  
  test "should get about page" do
    #get static_pages_about_url  # Before Named Routes 
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end
  
  test "should get contact page" do
    #get static_pages_contact_url  # Before Named Routes 
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end

end
